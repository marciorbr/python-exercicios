'''
Programa que recebe do usuário uma lista de nomes, separando os mesmo e
organizando por ordem alfabética, em seguida mostra os nomes organizados
'''

nomes = [x for x in input('Digite os nome separados por ,: ').split(',')]

nomes.sort()

print(','.join(nomes))