'''
Programa que recebe do usuário um número de 0 à 100 e transcreve ele por extenso
'''

from ntpath import join


def num_por_extenso (num):
    
    if num == 0:
        return 'zero'
    
    unidade = ("", "um", "dois", "três", "quatro", "cinco", "seis", "sete", "oito", "nove")
    
    dezena = ("", "", "vinte e ", "trinta e ", "quarenta e ", "cinquenta e ", "sessenta e ", "setenta e ", "oitenta e ", "noventa e ")
    
    dez_x = ("dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezessete", "dezoito", "dezenove")
    
    h, d, u = '', '', ''
    
    if num == 100:
        h = unidade[0] + 'cem'
        num = num % 100
        
    elif num >= 10 and num  < 20:
        d = dez_x[num - 10]
        num = 0
        
    elif num >= 20:
        d = dezena[num // 10]
        num = num % 10
        
    u = unidade[num]
    return ''.join(filter(None,[h,d,u]))
        
num = int(input('Digite um número: '))

if num <= 100:
    resultado = num_por_extenso(num)
    print(f'O {num} por extenso é: {resultado}')
else:
    print('Por Favor! Digite um número entra 0 e 100.')