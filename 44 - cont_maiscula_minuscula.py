'''
Programa que contará quantidade de letras maiúscula e minúscula de uma palavra digitada por uma usuário
'''

maiscula = 0
minuscula = 0

palavra = str(input('Digite uma palavra: '))

for letra in palavra:
    if (letra.islower()):
        minuscula += 1
    elif (letra.isupper()):
        maiscula += 1
        
print(f'A palavra tem {maiscula} letras maiscula e {minuscula} minuscula.')