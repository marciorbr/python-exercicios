'''
Lista de médicos a ser consultados, iteração com usuário que
realizará agendamento da consulta com base na lista de médicos
apenas para pacientes cadastrado préviamentes cadastrados
'''

medicos = {'1': 'Dr. João', '2': 'Dr. Ana'}

pacientes = {'001': 'Bruno','002': 'Carla', '003': 'Marcio' }

menu = str(input('Deseja agendar uma consulta? S/N ')).upper()

if menu == 'S':
    usuario = str(input('Digite seu número de usuário. '))
    if usuario in pacientes.keys():
        print('Lista de médicos disponíveis. ')
        for k, v in medicos.items():
            print(f'{k} - {v}')
        medico = int(input('Com qual médico(a) da lista deseja agendar sua consulta? '))
    
        if medico == 1:
            print(f'Consulta agendada com {medicos["1"]}')
        else:
            print(f'Consulta agendada com {medicos["2"]}')
    else:
        print('Usuário não cadastrado!')
else:
    print('Agradecemos seu contato.')