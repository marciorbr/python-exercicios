'''
Criar função que receba parêametros tanto por justaposição(*args), como nomeados(**kwargs) e print os valores recebidos.
'''

from re import M


def identificacao(*args, **kwargs):
    
    for n in args:
        nome = n
        
        for k, v in kwargs.items():
            idade = k
            sexo = v
            
            print(f'Nome: {nome}, {idade}, {sexo}')
            
pessoa = identificacao('Márcio', idade = 40, sexo = 'M')