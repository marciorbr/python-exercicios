'''
Molde para cadastro de veiculos, tendo como caracteristicas marca, modelo, ano, valor e cor
cadatrar 3 veículos, revelando seu identificador na memória e retorna os dados quando consultado
'''

class Veiculo:
    
    def __init__(self, marca, modelo, ano, cor, valor):
        self.marca = marca
        self.modelo = modelo
        self.ano = ano
        self.cor = cor
        self.valor = valor
        
    def consutar_veiculo(self):
        descricao_veiculo = f'{self.marca}, {self.modelo}, {self.ano}, {self.cor}, {self.valor}'
        return descricao_veiculo
    
carro1 = Veiculo('Audi', 'A3', '2006', 'preto', '39.999')
carro2 = Veiculo('Fiat', 'Uno', '2000', 'branco', '11.300')
carro3 = Veiculo('honda', 'City', '2018', 'cinza', '89.900')

print(carro1)
print(carro2)
print(carro3)

print(carro1.consutar_veiculo())
print(carro2.consutar_veiculo())
print(carro3.consutar_veiculo())