'''
Programa que converta um número inteiro digitado pelo usuário e o converta no formato romano
'''

class Conversor:
    
    def inteiro_para_romano(self, num):
        
        val_int = [
            1000, 900, 500, 400,
            100, 90, 50, 40, 
            10, 9, 5, 4, 
            1
        ]
        
        val_rom = [
            "M", "CM", "D", "CD", 
            "C", "XC", "L", "LX",
            "X", "IX", "V", "IV",
            "I"
        ]
        
        num_romano = ''
        
        i = 0
        
        while num > 0:
            for _ in range(num // val_int[i]):
                num_romano += val_rom[i]
                num -= val_int[i]
                
            i += 1
            
        return num_romano
    
num = int(input('Digite o número a ser convertido: '))

print(f'O número inteiro {num} convertido para romano é {Conversor().inteiro_para_romano(num)}')