'''
Encontrar todos os números divisíveis por 7 e não multiplos de 5 em um
intervalo de numeros, retornar os números em sequências e separados por ","
'''

lista = []

for i in range(2000, 2201):
    if i % 7 == 0 and i % 5 != 0:
        lista.append(str(i))
        resultado = ','.join(lista)
        
print(resultado)