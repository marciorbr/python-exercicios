'''
Programa que recebe 3 números e devolve o maior
Utilizar funções aninhadas para realizar a comparação
'''

x = int(input('Digite o primeiro número: '))
y = int(input('Digite o segundo número: '))
z = int(input('Digite o terceiro número: '))

def maior_de_dois(x, y):
    if x > y:
        return x
    else:
        return y
    
def maior_de_tres(x,y,z):
    return maior_de_dois(x, maior_de_dois(y,z))

print(f'O mario de {x}, {y}, {z} é: {maior_de_tres(x,y,z)}')