'''
Programa para gerar salário de um funcionário considerando apenas as horas trabalhadas
e horas extras trabalhadas, sendo o salário valor fixo R$ 29,11 e da hora extra de R$ 5,00
Crie uma regra onde o funcionário só tem hora extra a receber a partir de 40h de forma convencional
Fazer a validação da entrada do usuário
'''

valor_hora = 29.11
valor_hora_extra = 5

def calcular_salario(horas, adicional):
    
    if horas > 40:
        valor_final = horas + adicional
        return valor_final 
    else:
        valor_final = horas
        return valor_final
    
try:
    
    horas = int(input('Digite o número de horas trabalhadas: ')) * valor_hora
    adicional = int(input('Digite o número de horas extras: ')) * valor_hora_extra
except:
    print('Digite apenas números')
    quit()
    
funcionario = calcular_salario(horas, adicional)
print(f'Salário base é: R$ {horas}')
print(f'Adicional de horas é: R$ {adicional}')
print(f'Remineração Total é: R$ {funcionario}')