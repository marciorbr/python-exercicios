'''
Programa para criar um array de 5 elementos do tipo int
exibir em tela todos os elementos do array, em seguida exiber individualmente todos os elementos pares
de acordo com sua posição de indice.
'''
from array import *

numeros = array('i', [5,10,15,20,25])

for i in numeros:
    print(i)
    if i % 2 == 0:
        print(f'{i} é um número par!')