'''
Programa que valida senha, com requisitos minimos,
numero de 0 a 9, maiúscula, minúscula, simbolo especial, letra de a à z, mínimo 6 máximo 16 caracteres
'''

from operator import le
import re

senha = input('Digite uma senha: ')

x = True

while x:
    if(len(senha) < 6 or len(senha) > 16):
        print('A senha deve conter de 6 à 16 caracteres')
        break
    elif not re.search("[a-z]", senha):
        print('A senha deve conter pelo menos uma letra minúscula')
        break
    elif not re.search("[0-9]", senha):
        print('A senha deve conter pelo menos um número de 0 à 9')
        break
    elif not re.search("[A-Z]", senha):
        print('A senha deve conter pelo menos uma letra maiúscula')
        break
    elif not re.search("[!@#$&*]", senha):
        print('A senha deve conter pelo menos um carater especial')
        break
    elif re.search("\s", senha):
        break
    else:
        print('Senha cadastrada com sucesso!!')
        x = False
        break
    
if x:
    print('Senha Inválida')