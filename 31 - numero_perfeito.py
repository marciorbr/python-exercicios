'''
programa que recebe do usuário um número e retorna se o número digitado e perfeito
'''

x = int(input('Digite um número: '))

def num_perfeito(x):
    soma = 0
    for i in range(1, x):
        if x % i == 0:
            soma += i
    return soma == x

if num_perfeito(x):
    print(f'{x} é um número perfeito')
else:
    print(f'{x} não e um número perfeito')