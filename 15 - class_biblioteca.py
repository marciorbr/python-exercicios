'''
Criar uma classe biblioteca que sirva de molde para de um livro de arcordo com o título,
porém espera um número indefinido de títulos.
'''

class Biblioteca:
    
    def __init__(self, livro1, **kwargs):
        self.livro1 = livro1
        
        
prateleira1 = Biblioteca('Livro Teste')

prateleira1.livro2 = 'Manual DevOps'
prateleira1.livro3 = 'Projeto Phoenix'
prateleira1.livro4 = 'A meta'
prateleira1.livro5 = 'Python em Exercicios'

print(prateleira1.livro4)
print(prateleira1.livro1)