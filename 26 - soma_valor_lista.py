'''
Somar valores de uma lista utilizando list comprehension
'''

# Forma tradicional
carrinho = []

carrinho.append(('Item 1', 30))
carrinho.append(('Item 2', 40))
carrinho.append(('Item 3', 20))
carrinho.append(('Item 4', 33))

total = 0

for produto in carrinho:
    total = total + produto[1]
    
print(f'Resultado Traducional {total}')

# Forma otimizada

total = sum([y for x, y in carrinho])

print(f'Resultado otimizdo {total}')