'''
programa que recebe uma frase e retorna a soma com a frequencia das palavras
'''

frequencia = {}

texto = 'Se alguma coisa pode dar errado, dará errado, e mais, dará errado da pior maneira, no pior momento e de modo que cause o pior impacto / dano possível.'

for palavra in texto.split():
    frequencia[palavra] = frequencia.get(palavra, 0) + 1
    
palavras = frequencia.keys()

for i in palavras:
    print(f'{i} = {frequencia[i]}')
