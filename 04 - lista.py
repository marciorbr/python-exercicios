lista = ['casa', 'carro', 'bola', 'gato', 'moto']

lista[2] = 'luva' # Substituir elemento na terceira posição

lista.append('giz') # Inserir um novo elemento ao final da lista

lista.insert(2,'olho') # Inserir um elemento na terceira posição

lista.remove('carro') # Remover carro da lista

print(lista) # Imprimir toda a lista

print(lista[-1]) # Imprimir último elemento da lista

print(lista[1:4]) # Imprimir 2, 3 e 4 elemento da lista

