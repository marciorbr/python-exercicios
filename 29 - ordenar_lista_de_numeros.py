'''
Programa que recebe números aleatorios de 0 a 10 armazena em uma lista e retorna os mesmo ordenados
'''

numeros = input('Digite números no intervalo de 0 a 10 separados por virgula: ')

lista = numeros.split(',')

lista.sort()

print(lista)