'''
Programa que recebe uma nota entre 0 e 1.0, classificando de acordo com a nota se o aluno foi aprovado
ou ficou em recuperação. A média para aprovação de ser >= 0.6, o programa deve fazer as validações de entrada do usuário
e formato da nota
'''

def calcular_nota(nota):
    
    if nota < 0 or nota > 1.0:
        print('Valor da nota inserido deve ser entra 0 e 1.0')
    elif nota == 1.0:
        print('Parabêns, você gabaritou a prova.')
    elif nota >= 0.6:
        print('Parabêns, você foi aprovado.')
    else:
        print('Você não foi aprovado.')
        
try:
    nota = float(input('Digite a nota de 0 a 1.0: '))
except:
    print('Formato da nota deve ser entra 0 e 1.0')
    quit()

calcular_nota(nota)