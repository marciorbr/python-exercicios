'''
Criar uma cauculadora simples de 4 operações utilizando OO
'''

class Calculadora:
    
    def soma(self, n1, n2):
        return n1 + n2
    
    def subitracao(self, n1, n2):
        return n1 - n2
    
    def divisao(self, n1, n2):
        return round(n1 / n2)
    
    def multiplicacao(self, n1, n2):
        return n1 * n2
    
calculo = Calculadora()

print(calculo.soma(1, 2))

print(calculo.divisao(4, 2))

print(calculo.subitracao(4, 1))

print(calculo.multiplicacao(2, 10))