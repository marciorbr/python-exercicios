def valor_ao_quadrado(valor):
    '''
    Função que ao receber uma número retorna o valor 
    ao quadrado.
    
    Ex: valor_ao_quadrado(16) retornará 256.
    '''
    
    return valor ** 2

print(valor_ao_quadrado.__doc__)