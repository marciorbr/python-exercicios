'''
O usuário digite um número qualquer, em seguida retorne se é primo ou não
caso não retorne quantas vezes e divisível
'''
numero = int(input('Digite seu número... '))
divisoes = 0

for i in range(1, numero + 1):
    if numero % i == 0:
        divisoes += 1
        
if divisoes == 2:
    print(f'Número {numero} é primo!')
    print(f'Número {numero} é divisível por 1 e por {numero}')
else:
    print(f'Número {numero} não é primo!')
    print(f'Número {numero} é divisível {divisoes} vezes')
