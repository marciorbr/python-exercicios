'''
Gerar um dicionário a partir de um número digitado pelo usuário
de modo que será apresentado todos os valores anteessores ao número
multiplicado por ele mesmo.
Ex: número digitado 4, resultado {1:1, 2:4, 3:9, 4:16}
'''

numero = int(input('Digite um número: '))

dicionario = dict()

for i in range(1, numero + 1):
    dicionario[i] = i * i
    
print(dicionario)