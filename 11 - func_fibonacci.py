'''
Programa que retorne o numero de fibonacci
Finonacci o número inicia em 1 ou 0 e cada número subseguente
corresponde a soma dos dois anteriores
'''

def fibonacci(num):
    if num <= 1:
        return num
    else:
        return fibonacci(num - 1) + fibonacci(num - 2)
    
num = int(input('Digite uma número: '))

resposta = fibonacci(num - 1)

print(resposta)