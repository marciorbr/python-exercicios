'''
programa mecanismo controlador de um sistema direcional, registrando a direção e quantidades de passos
ao final exibirar em tela o numero de referência para uma plano cartesiano.
'''

import math

posicao = [0,0]

while True:
    
    coordenada = input('Digite a cordenada "CIMA,BAIXO,DIREITA,ESQUERDA" seguida do número de passos: ')
    if not coordenada:
        break
    
    movimento = coordenada.split(' ')
    direcao = movimento[0]
    passos = int(movimento[1])
    
    if direcao == 'CIMA':
        posicao[0] += passos
    elif direcao == 'BAIXO':
        posicao[0] -= passos
    elif direcao == 'ESQUERDA':
        posicao[1] -= passos
    elif direcao == 'DIREITA':
        posicao[1] += passos
    else:
        pass
    
print(int(round(math.sqrt(posicao[1] ** 2 + posicao[0] ** 2))))