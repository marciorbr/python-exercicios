from datetime import datetime
'''
Programa que conta todas as segundas que caem no primeiro dia do mês entre os anos de 2010 à 2021.
'''

from pyexpat.errors import messages

segundas = 0

meses = range(1, 13)

for ano in range(2010, 2021):
    for mes in meses:
        if datetime(ano, mes, 1).weekday() == 0:
            segundas += 1
            
print(f'Entre 2010 e 2021 existem {segundas} segundas-feiras no primeiro dia do mês.')