'''
Programa para formatar data atual retornando no formato dia, mês e ano
'''

import datetime


data_atual = datetime.datetime.now()

print('Data e hora atual')
print(data_atual.strftime('%d-%m-%Y, %H:%M:%S'))