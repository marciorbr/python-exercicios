'''
programa que gera um número aleatório de 0 á 10, salvando esse número em uma variável
onde o usuário tentará adivinhar o número até acertar, quando acertar retorne uma menssagem de parabêns com o número gerado.
'''
from random import randint

num_aleatorio = randint(0, 11)
cond = True

while cond:
    
    numero = int(input('Digite um número de 0 a 10 para tentar adinhar: '))
    
    if numero == num_aleatorio:
        print(f'Parabêns, você acertou o número é {num_aleatorio}')
        cond = False