from operator import itemgetter

nomes = []

while True:
    dados = input('Digite seu nome, seguido de duas notas: ')
    if not dados:
        break
    nomes.append(tuple(dados.split(",")))
    
print(sorted(nomes, key=itemgetter(0,1)))