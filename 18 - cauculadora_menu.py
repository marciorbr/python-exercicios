'''
Calculadora simples de 4 operações onde o usuário escolherá umas das 4 
operacões (+ - * /), sendo que, o usuário digitará apenas 2 números para calculo
'''

print('CALCULADORA MATEMÁTICA DE 4 OPERAÇOES(+ - * /)\n')

num1 = int(input('Digite o primeiro número: '))
num2 = int(input('Digite o segundo número: '))
ope = str(input('Digite a operção (+ - * /): '))

def soma(n1, n2):
    return n1 + n2

def subtracao(n1, n2):
    return n1 - n2

def divisao(n1, n2):
    return n1 / n2

def multiplicacao(n1, n2):
    return n1 * n2

if ope == '+':
    print(f'O resultado da operação é {num1} + {num2} = {soma(num1, num2)}')
elif ope == '-':
        print(f'O resultado da operação é {num1} - {num2} = {subtracao(num1, num2)}')
elif ope == '/':
        print(f'O resultado da operação é {num1} / {num2} = {divisao(num1, num2)}')
elif ope == '*':
        print(f'O resultado da operação é {num1} * {num2} = {multiplicacao(num1, num2)}')
else:
    print('Tipo de operação escolhida é inválida!!!')