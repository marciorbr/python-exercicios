base = {
    'Pergunta01': {
        'pergunta': 'Quanto é 4 x 4 ?',
        'alternativas': {'a':'13', 'b': '14', 'c': '16', 'd': '24'},
        'resposta': 'c',
    },
    'Pergunta02': {
        'pergunta': 'Quanto é 6 / 3 ?',
        'alternativas': {'a':'1', 'b': '4', 'c': '3', 'd': '2'},
        'resposta': 'd',        
    },
}

respostas_certas = 0

for pkey, pvalue in base.items():
    print(f'{pkey}: {pvalue["pergunta"]}')
    for rkey, rvalues in pvalue["alternativas"].items():
        print(f'{rkey}: {rvalues}')
        
    cond = True
    while cond is True:
        reposta = input('Escolha uma alternativa: ')
        if reposta not in ['a', 'b', 'c', 'd']:
            print('Alternativa inválida!!')
        else:
            cond = False
    
    if reposta == pvalue["resposta"]:
        print('Resposta certa!')
        respostas_certas += 1
    else:
        print('Resposta errada!')
        
if respostas_certas == 0:
    print('Você não acertou nenhuma resposta.')
elif respostas_certas == 1:
    print('Você acertou 1 resposta correta.')
else:
    print('Você acertou 2 respostas corretas.')