'''
Class que recebe do usuário uma string qualquer e retorna a mesma em maiúscula
'''

class Conversor(object):
    
    def __init__(self):
        self.string = ''
        
    def receber_string(self):
        self.string = str(input('Digite a String: '))
        
    def converter_maiuscula(self):
        print(self.string.upper())
        
        
frase = Conversor()
frase.receber_string()
frase.converter_maiuscula()