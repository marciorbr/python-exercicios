'''
Contar numeros de 1 a 100, usando apenas de numeros impares, 
ao final exibir quantos numeros impares foram encontrados no intervalo 
e a soma total dos mesmos.
'''

cont = 0
soma = 0

for i in range(1, 101):
    if i % 3 == 0:
        soma += i
        cont += 1
        
print(f'Total de números impar é {cont} e o a soma é {soma}')