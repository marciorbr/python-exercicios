'''
Programa que a partir de uma lista numérica seja gerado ou lista usando list comprehension
onde a nova lista seja formada pelos elementos da primeira elevado ao cubo
'''

lista = [1,2,3,4,5,6]

nova_lista = [ i ** 3 for i in lista]

print(lista)
print(nova_lista)