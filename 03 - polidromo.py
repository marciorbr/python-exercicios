'''
Usuário digitar um nome ou uma frase, verificar se o conteúdo digitado é um polídromo.
'''

# strip - remove espaços em brancos tanto no começo como no final da frase
# upper - converte todos os caracteres em maiúsculo
frase = str(input('Digite um nome ou uma frase: ')).strip().upper()
palavra = frase.split()
caracteres = ''.join(palavra)
frase_invertida = ''

for i in range(len(caracteres)-1, -1, -1):
    frase_invertida += caracteres[i]

if frase_invertida == caracteres:
    print(f'A Frase "{frase}" é uma Polídromo.')
else:
    print(f'A frase "{frase}" não é um Polídromo')
print()
print(caracteres, frase_invertida)