# Criar um dicionario utilizando método construtor dict()
dicionario = dict(
    pao = '1',
    queijo = '2',
    presunto = '3',
    manteiga = '4',
)
print(dicionario) # Imprimindo dicionário

# Criar um dicinario e alimentar com 2 listas
chave = [1, 2, 3, 4, 5]
valor = ['pao', 'queijo', 'presunto', 'leite', 'cafe']

dicionario1 = dict(keys=chave, value=valor)
print(dicionario1)

# Uma função que recebe uma lista de dados de alunos como um dicionário e calcula a média das notas e devolve uma lista com dicionário com nome e média de cada aluno
aluno = [{'Nome':'Fernando', 'Notas': [62, 73, 90]}, {'Nome':'Marcio', 'Notas': [80, 73, 90]}]

def calcula_media(aluno):
    notas=[]
    for media in aluno:
        
        if len(media['Notas']) > 0:
            temp = round(sum(media['Notas'])/len(media['Notas']))
        else:
            temp = 0
        notas.append({'Nome':media['Nome'], 'Média Nota': temp })
    print(notas)
        
media_estudante = calcula_media(aluno)