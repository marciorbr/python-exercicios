'''
Programa que recebe nome e sobre nome do usuário e trasnforma em padrão americano, inverterno os mesmo
'''


def invert_nome(n):
    nome, sobre_nome = n.split()
    return ' '.join([sobre_nome,nome])

n = str(input('Digite seu nome e sobre nome: '))

print(f'Seu nome em padrão americano é; {invert_nome(n)}')