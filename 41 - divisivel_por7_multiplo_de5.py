'''
Programa que encontra todos os números divisíveis por 7 e multiplo de 5
de uma ranger de 0 a 500 (incluindo tais valores).
'''

numeros = []

for numero in range(0,501):
    
    if (numero % 7 == 0) and (numero % 5 == 0):
        numeros.append(str(numero))
        
print(','.join(numeros))