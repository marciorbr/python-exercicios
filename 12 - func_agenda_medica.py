'''
Lista de médicos a ser consultados, iteração com usuário que
realizará agendamento da consulta com base na lista
'''

medicos = {'1': 'Dr. João', '2': 'Dr. Ana'}

menu = str(input('Deseja agendar uma consulta? S/N ')).upper()

if menu == 'S':
    paciente = str(input('Digite seu nome completo. '))
    print('Lista de médicos disponíveis. ')
    for k, v in medicos.items():
        print(f'{k} - {v}')
    medico = int(input('Com qual médico(a) da lista deseja agendar sua consulta? '))
    
    if medico == 1:
        print(f'Consulta agendada com {medicos["1"]}')
    else:
        print(f'Consulta agendada com {medicos["2"]}')
        
else:
    print('Agradecemos seu contato.')