'''
Programa que recebe uma palavra qualquer, aplicando sobre a mesma uma formatação de estilo e retornando o estilo aplicado negrito, itálico e sublinhado
as funções que aplicam o estilo devem ser separadas entre si, porém executadas em conjunto via decoradores para uma função principal.
'''

def negrito(palavra):
    
    def transformar():
        return "<b>" + palavra() + "</b>"
    
    return transformar

def italico(palavra):
    
    def transformar():
        return "<i>" + palavra() + "</i>"
    
    return transformar

def sublinhado(palavra):
    
    def transformar():
        return "<u>" + palavra() + "</u>"
    
    return transformar

@negrito
@italico
@sublinhado
def aplicar_estilo():
    palavra = str(input('Digite uma palavra: '))
    return palavra

print(aplicar_estilo())