'''
Criar uma função de números de parâmetros indefinido que realizará a soma de todos os números passados.
'''

def soma(*args):
    num = 0
    for n in args:
        num += n
        
    print(f'O resultado da soma é {num}' )
    
soma = soma(18, 43, 99, 1)